<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/tasks', 'TaskController@task');
Route::post('/createTask', 'TaskController@createTask');
Route::put('/updateTask/{id}','TaskController@updateTask');
Route::get('/selectTask/{id}', 'TaskController@selectTask');
Route::delete('/removeTask/{id}', 'TaskController@deleteTask');

Route::get('/alluser','UserController@allusers');
Route::get('/user/{id}', 'UserController@user');

Route::post('/reply','ReplyController@addReply');
Route::get('/getReply/{id}','ReplyController@getReply');