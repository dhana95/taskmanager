<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{
    public function task(){
        $tasks = DB::table('tasks')->join('users','users.id','=','tasks.userid')->select('tasks.*', 'users.name')->get();
        //return view('welcome', compact('tasks'));
        return $tasks;
    }
    public function createTask(Request $request){
        $title = $request->input('title');
        $description = $request->input('description');
        $userid = $request->input('uid');
        DB::table('tasks')->insert(
            ['title'=>$title, 'description'=>$description, 'userid'=>$userid]
        );
        return $title;
    }
    public function updateTask($id, Request $request){
        $title = $request->input('title');
        $des =$request->input('description');
        $userid = $request->input('uid');
        DB::table('tasks')->where('id', $id)
                          ->update(['title'=> $title, 'description'=>$des, 'userid'=>$userid]);     
    }
    
    public function selectTask($id){
        $tasks = DB::table('tasks')->where('id', $id)->get();
        //var_dump($tasks);
       //echo $id;
       return $tasks;
    }
    public function deleteTask($id){
        DB::table('tasks')->where('id', $id)->delete();
    }

    public function orderbyTask(){
        $tasks = DB::table('tasks')->join('users','users.id','=','tasks.userid')->select('tasks.*', 'users.name')->orderBy('users.name','desc')->get();
        //return view('welcome', compact('tasks'));
        return $tasks;
    }
}
