<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\reply;

class ReplyController extends Controller
{
    public function addReply(Request $request){
        $reply = new reply;
        $reply->description = $request->description;
        $reply->taskid = $request->taskid;
        $reply->save();
    }
    public function getReply($id){
        $reply = new reply;
        $replies = $reply->where('taskid',$id)->get();
        return $replies;
    }
}
