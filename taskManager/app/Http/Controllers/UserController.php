<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function allusers(){
        $users =  DB::table('users')->select('name')->get();
        return $users;
    }
    public function user($id){
        $user = DB::table('users')->select('name')->where('id', $id)->get();
        return $user;
    }
}
