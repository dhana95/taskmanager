import React, { Component } from 'react'
import axios from 'axios'

export default class updateTask extends Component {
    constructor(props){
        super(props);
        console.log(props.match.params.id);
        this.state = {
            title:'',
            description:'',
            uid:''
        }
    }
    componentDidMount(){
        axios.get('/api/selectTask/'+this.props.match.params.id).then((response)=>{
            console.log(response.data[0].title);
            this.setState({
                id: response.data[0].id,
                title: response.data[0].title,
                description: response.data[0].description,
                uid:response.data[0].userid
            })
        })
    }
    getTitle(title){
        this.setState({title:title});
    }
    getDescription(descript){
        this.setState({description:descript});
    }
    getuser(uid){
        this.setState({uid:uid});
    }
    getAll(event){
        event.preventDefault();
        let dataset = {
            title: this.state.title,
            description: this.state.description,
            uid:this.state.uid
        }
        axios.put("/api/updateTask/"+this.state.id, dataset).then(function(response){
            alert('updated successfully');    
            console.log(response.statusText);
            
        }).catch(function(error){
            console.log(error);
        });
    }
    render() {
    return (
        <form type="post" onSubmit={this.getAll.bind(this)} >
                <div className="form-group">
                    <label>Title</label>
                    <input value={this.state.title} type="text" required onChange={(event)=>this.getTitle(event.target.value)} className="form-control" name="title"/>
                </div>
                <div className="form-group">
                    <label>Description</label>
                    <input value={this.state.description} type="text" required className="form-control" onChange={(event)=>this.getDescription(event.target.value)} name="description"/>
                </div>
                <div className="form-group">
                    <label>Select user</label>
                    <select value={this.state.uid} required name="uid" onChange={(event)=>this.getuser(event.target.value)} className="form-control">
                        <option value='1'>Kavinda</option>
                        <option value='2'>Pasindu</option>
                        <option value='3'>Navod</option>
                    </select>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
    )
  }
}
