import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Tasks from './Tasks';
import createTask from './createTask';
import { Link } from 'react-router-dom';
import updateTask from './updateTask';
import replyTask from './replyTask';
import allReply from './getReply';

export default class Example extends Component {
    render() {
        return (
            <BrowserRouter>
                <div>
                    <nav className="navbar navbar-expand-md fixed-top navbar-light bg-light">
                        <div >
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link className="nav-link" to="/">home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/create">Creat Task</Link>
                            </li>
                        </ul>
                        </div>                        
                    </nav>
                    <br></br>
                    <br></br>
                    <br></br>
                    <div className="container">
                    <Switch>
                        <Route exact path='/' component={Tasks}/>
                        <Route path='/create' component={createTask}/>
                        <Route path='/update/:id' component={updateTask}/>
                        <Route path='/reply/:id' component={replyTask}/>
                        <Route path='/allReplies/:id' component={allReply}/>
                    </Switch>
                    </div>
                    
                </div>
            </BrowserRouter>
        );
    }
}

if (document.getElementById('root')) {
    ReactDOM.render(<Example />, document.getElementById('root'));
}
