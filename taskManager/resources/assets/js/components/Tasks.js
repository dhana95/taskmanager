import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';

export default class Tasks extends Component {
    constructor(){
        super()
        this.state = {
            tasks: [],
            name: []
        }
    }
    componentDidMount(){
        axios.get('api/tasks')
            .then((response)=>{
                this.setState({tasks: response.data});
            });
    }
    
    redirect(){
        this.props.history.push('/');
    }
    removeTasks(id){
        console.log(id);
        axios.delete('api/removeTask/'+id).then((response)=>{
            alert('data deleted');
            console.log(response);

        })
        return(
            <Fragment>
                {this.redirect()}
            </Fragment>
        );
    }

    // getUser(id){
    //     axios.get('/api/user/'+id).then((response)=>{
    //         console.log(response.data)
    //         this.setState({
    //             name: response.data
    //         });
    //     })
    // }
    
    render() {
        
    return (
      <table className="table">
          <thead>
              <td>Title</td>
              <td>Description</td>
              <td>user id</td>
              <td>Reply</td>
              <td>Edit</td>
              <td>Delete</td>  
          </thead>
          <tbody>
              {
                this.state.tasks.map(task=>{
                    return(
                    <tr key={task.id}>
                        <td><Link to={`/allReplies/${task.id}`}>{task.title}</Link></td>
                        <td>{task.description}</td>
                        {/* <td>{this.getUser(task.userid)}</td> */}
                        <td>{task.name}</td>
                        <td><Link className="btn btn-primary" to={`/reply/${task.id}`}>Reply</Link></td>
                        <td><Link className="btn btn-success" to={`/update/${task.id}`}>Edit</Link></td>
                        <td><button className="btn btn-danger" value={task.id} onClick={()=>this.removeTasks(task.id)}>Delete</button></td>
                    </tr>
                    );
                  
                })
              }
          </tbody>
      </table>
    )
  }
}
