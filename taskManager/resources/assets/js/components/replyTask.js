import React, { PureComponent } from 'react'
import axios from 'axios';

export default class replyTask extends PureComponent {
    constructor(props){
        super(props);
        console.log(props);
        this.state={
            title:'',
            description:'',
            reply:'',
            taskid:''
        }
    }
    componentDidMount(){
        axios.get('/api/selectTask/'+this.props.match.params.id).then((response)=>{
            console.log(response);
            this.setState({
                title: response.data[0].title,
                description: response.data[0].description,
                taskid: response.data[0].id
            });
        })
        
    }
    getReply(reply){
        this.setState({reply:reply});
    }

    addReply(event){
        event.preventDefault();
        let dataset = {
            description: this.state.reply,
            taskid: this.state.taskid
        }
        axios.post("/api/reply",dataset).then(function(response){
            alert('Reply added');
            console.log(response.statusText);
        })
    }
    render() {
    return (
      <div className="row mb-2">
        <div className="col-md-12">
            <div className="card flex-md-row mb-4 shadow-sm h-md-250">
                <div className="card-body d-flex flex-column align-items-start">
                    <h4 className="mb-0">
                        <a className="text-dark" href="#">{this.state.title}</a>
                    </h4>
                    <p className="card-text mb-auto">
                        {this.state.description}
                    </p>
                </div>
            </div>
        </div>
        <div className="col-md-10">
            <div className="card flex-md-row mb-4 shadow-sm h-md-250">
                <form method="post" onClick={this.addReply.bind(this)} >
                    <input type="text" onChange={(event)=>this.getReply(event.target.value)} className="form-control" placeholder="Add a reply" />
                    <button className="btn btn-primary btn-sm float-right" type="submit">Add</button>
                </form>
            </div>
        </div>
      </div>
    )
  }
}
