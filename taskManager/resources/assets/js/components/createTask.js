import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

export default class createTask extends Component {
    constructor(){
        super()
        this.state = {
            'title': '',
            'description':'',
            'userid':'',
            'username':'',
            'redirect': false
        }
    }
    componentDidMount(){
        axios.get('api/user/{id}').then((response)=>{
            this.setState({username:reponse.data.name});
        });
    }
    getTitle(title){
        this.setState({title:title});
    }
    getDescription(descript){
        this.setState({description:descript});
    }
    getuser(uid){
        this.setState({userid:uid});
    }
    setRedirect() {
        this.setState({
            redirect: true
        });
    }
    getAll(event){
        event.preventDefault();
        let dataset = {
            title: this.state.title,
            description: this.state.description,
            uid:this.state.userid
        }
        axios.post("api/createTask", dataset).then(function(response){
            alert('new record created');    
            console.log(response.statusText);
            
        }).catch(function(error){
            console.log(error);
        });
    }
    render() {
        return (
            <form type="post" onSubmit={this.getAll.bind(this)}>
                <div className="form-group">
                    <label>Title</label>
                    <input type="text" required onChange={(event)=>this.getTitle(event.target.value)} className="form-control" name="title"/>
                </div>
                <div className="form-group">
                    <label>Description</label>
                    <input type="text" required className="form-control" onChange={(event)=>this.getDescription(event.target.value)} name="description"/>
                </div>
                <div className="form-group">
                    <label>Select user</label>
                    <select required name="uid" onChange={(event)=>this.getuser(event.target.value)} className="form-control">
                        <option value='1'>Kavinda</option>
                        <option value='2'>Pasindu</option>
                        <option value='3'>Navod</option>
                    </select>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        )
    }
}
