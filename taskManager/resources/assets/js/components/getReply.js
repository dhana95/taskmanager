import React, { PureComponent } from 'react'
import axios from 'axios';

export default class getReply extends PureComponent {
    constructor(props){
        super(props);
        console.log(props.match.params.id);
        this.state={
            taskTitle:'',
            taskDescription:'',
            replyDes:[]
        }
    }
    componentDidMount(){
        axios.get('/api/selectTask/'+this.props.match.params.id).then((response)=>{
              console.log(response);
              this.setState({
                  taskTitle: response.data[0].title,
                  taskDescription: response.data[0].description
              });  
        });
        axios.get('/api/getReply/'+this.props.match.params.id).then((response)=>{
            console.log(response.data);
            this.setState({
                replyDes:response.data
            });
            console.log(this.state.replyDes);
        })
    }
  render() {
    return (
      <div className="card">
        <div className="card-body">
            <h5 className="card-title">{this.state.taskTitle}</h5>
            <p>{this.state.taskDescription}</p>
        </div>
        <div className="card">
            <div className="card-body">
                <h6 className="card-title">Replies</h6>
                   {this.state.replyDes.map((reply)=>{
                       return(
                           <div className="card">
                                <div className="card-body">
                                {reply.description}
                                </div>
                           </div>
                       );
                   }
                    
                   )}
            </div>
        </div>
      </div>
    )
  }
}
